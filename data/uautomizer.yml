name: UAutomizer
input_languages:
  - C
project_url: https://ultimate-pa.org
repository_url: https://github.com/ultimate-pa/ultimate
spdx_license_identifier: LGPL-3.0-or-later
coveriteam_actor: uautomizer
benchexec_toolinfo_module: ultimateautomizer.py
fmtools_format_version: "2.0"
fmtools_entry_maintainers:
  - danieldietsch

maintainers:
  - name: Matthias Heizmann
    institution: University of Freiburg
    country: Germany
    url: https://swt.informatik.uni-freiburg.de/staff/heizmann
  - name: Dominik Klumpp
    institution: University of Freiburg
    country: Germany
    url: https://swt.informatik.uni-freiburg.de/staff/klumpp
  - name: "Frank Sch\xFCssele"
    institution: University of Freiburg
    country: Germany
    url: https://swt.informatik.uni-freiburg.de/staff/schuessele
  - name: Daniel Dietsch
    institution: University of Freiburg
    country: Germany
    url: https://swt.informatik.uni-freiburg.de/staff/dietsch
versions:
  - version: svcomp24
    doi: 10.5281/zenodo.10085306
    benchexec_toolinfo_options: [--full-output]
    required_ubuntu_packages:
      - openjdk-11-jre-headless
  - version: svcomp24-correctness
    doi: 10.5281/zenodo.10085306
    benchexec_toolinfo_options:
      [--full-output, --witness-type, correctness_witness]
    required_ubuntu_packages:
      - openjdk-11-jre-headless
  - version: svcomp24-violation
    doi: 10.5281/zenodo.10085306
    benchexec_toolinfo_options:
      [--full-output, --witness-type, violation_witness]
    required_ubuntu_packages:
      - openjdk-11-jre-headless
  - version: svcomp23
    url: https://gitlab.com/sosy-lab/sv-comp/archives-2023/-/raw/main/2023/uautomizer.zip
    benchexec_toolinfo_options: [--full-output]
    required_ubuntu_packages:
      - openjdk-11-jre-headless

competition_participations:
  - competition: SV-COMP 2024
    track: Verification
    tool_version: svcomp24
    jury_member:
      name: Matthias Heizmann
      institution: University of Freiburg
      country: Germany
      url: https://swt.informatik.uni-freiburg.de/staff/heizmann
  - competition: SV-COMP 2024
    track: Validation of Correctness Witnesses
    tool_version: svcomp24-correctness
    jury_member:
      name: Matthias Heizmann
      institution: University of Freiburg
      country: Germany
      url: https://swt.informatik.uni-freiburg.de/staff/heizmann
  - competition: SV-COMP 2024
    track: Validation of Violation Witnesses
    tool_version: svcomp24-violation
    jury_member:
      name: Matthias Heizmann
      institution: University of Freiburg
      country: Germany
      url: https://swt.informatik.uni-freiburg.de/staff/heizmann
  - competition: SV-COMP 2023
    track: Verification
    tool_version: svcomp23
    jury_member:
      name: Matthias Heizmann
      institution: University of Freiburg
      country: Germany
      url: https://swt.informatik.uni-freiburg.de/staff/heizmann
techniques:
  - CEGAR
  - Predicate Abstraction
  - Bit-Precise Analysis
  - Lazy Abstraction
  - Interpolation
  - Automata-Based Analysis
  - Concurrency Support
  - Ranking Functions
  - Algorithm Selection
  - Portfolio
