#!/bin/bash

# Enable exit on error
set -euo pipefail

echo "Calculating files that were changed by the MR."

if [[ -n "${CI_MERGE_REQUEST_DIFF_BASE_SHA:-}" ]]; then
  # GitLab CI pipeline for MR
  # GitLab tells us exactly what we need to compare against, just make sure it is present locally.
  git fetch origin "$CI_MERGE_REQUEST_DIFF_BASE_SHA"
  DIFF_BASE=$CI_MERGE_REQUEST_DIFF_BASE_SHA

else
  if [[ "${CI_PROJECT_PATH:-}" = sosy-lab/* ]]; then
    # GitLab CI pipeline (not for MR) in main repo
    # We just compare against main branch after making sure it is present locally.
    TARGET_REPO=origin
    TARGET_BRANCH="$CI_DEFAULT_BRANCH"

  elif [[ -n "${CI_PROJECT_PATH:-}" ]]; then
    # GitLab CI pipeline (not for MR) in fork
    # Compare against main branch of upstream project, but we need to fetch it first.
    git remote add upstream https://gitlab.com/sosy-lab/benchmarking/fm-tools.git || git remote set-url upstream https://gitlab.com/sosy-lab/benchmarking/fm-tools.git
    TARGET_REPO=upstream
    TARGET_BRANCH=main

  else
    # Not in GitLab Ci
    # We just compare against main branch after making sure it is present locally,
    # assuming that the user has a standard git checkout with "origin".
    TARGET_REPO=origin
    TARGET_BRANCH=main
  fi

  DIFF_BASE="$TARGET_REPO/$TARGET_BRANCH..."

  # In order to compute the merge base, git needs not only the relevant trees,
  # but also the history. Let's fetch 200 commits back from the tips of the
  # current branch the the comparison branch and hope this is enough.
  git fetch --depth=200 origin HEAD
  git fetch --depth=200 "$TARGET_REPO" "$TARGET_BRANCH"
fi

echo "Comparing against '$DIFF_BASE'."
CHANGED_YML_FILES=$(git diff --name-only --diff-filter=d "$DIFF_BASE" -- data/*.yml)
echo "Changed YML files:"
echo "$CHANGED_YML_FILES"
